pragma solidity ^0.5.1;

contract MyContract {
    
    event Purchase (
        address indexed _buyer,
        uint256 _amount
    );
    
    mapping(address => uint256) public balances;
    
    Person[] public people;
    
    mapping(uint => Person) public peoples;
    
    enum State {Waiting, Ready, Active }
    
    uint256 public peopleCount;
    
    uint256 public openingTime = 1592141404;
    
    address owner;
    address payable wallet;
    
    State public state;
    
    struct Person {
        uint _id;
        string _firstName;
        string _lastName;
    }
    
    constructor(address payable _wallet) public {
        state = State.Waiting;
        owner = msg.sender;
        wallet = _wallet;
    }
    
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
    
    modifier onlyWhileOpen() {
        require(block.timestamp >= openingTime);
        _;
    }
    
    function buyToken() public payable {
        //buy a Token
        balances[msg.sender] += 1;
        //send ether to the wallet
        wallet.transfer(msg.value);  //metadaga to set value 
        emit Purchase(msg.sender,1);
    }
    
    string public constant value = "MyValue";
    bool public myBool = true;
    string public stringvalue = "MyValue";
    int public myInt = -1;
    uint public myuInt = 1;
    uint8 public myu8Int = 1;
    uint256 public muyInt256 = 1;
    
    function set(string memory _value) public {
        stringvalue = _value;
    }  
    
    function activate() public {
        state = State.Active;
    }
    
    function isActive() public view returns(bool) {
        return state == State.Active;
    }
    
    function addPerson(string memory _firstName, string memory _lastName) public onlyWhileOpen {
        incrementCount();
        people[peopleCount] = Person(peopleCount, _firstName, _lastName);
    }
    
    function incrementCount() internal {
        peopleCount += 1;
    }
    
    //fallback function
    //ico s cases
    function() external payable {
        buyToken();
    }
}
