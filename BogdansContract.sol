pragma solidity ^0.5.1;

contract ERC20Token {
    string public name;
    mapping(address => uint256) public balances;
    
    function mint() public {
        balances[tx.origin] ++;
    }
}

contract MyContract {
    
    address payable wallet;
    address public token;
    
    constructor(address payable _wallet, address _token ) public {
        wallet = _wallet;
        token = _token;
    }
    
    function buyToken() public payable {
        //buy a Token
        //send ether to the wallet
        ERC20Token(address(token)).mint();
        wallet.transfer(msg.value);  //metadaga to set value 
    }
    
    //fallback function
    //ico s cases
    function() external payable {
        buyToken();
    }
}
